#!/bin/bash
# Author: karni prabhu
# Date: 16/04/2021
# Description: release notes files merging
# modified:

merge=/root/anudan-project/anudan-app/frontend-backend-release-notes.txt
if [ -f "$merge" ]
 then
   rm $merge
fi
/root/anudan-project/anudan-app/anudan-app-release-notes.sh
cd /root/anudan-project/gms-service && ./gms-service-release-notes.sh


cd /root/anudan-project/anudan-app && cat /root/anudan-project/anudan-app/anudan-app-release-notes.txt  /root/anudan-project/gms-service/gms-service-release-notes.txt >> frontend-backend-release-notes.txt
