#!/bin/bash
#This script will generate the release notes from the commits
#It will discard prints oF automatic Merges and Pull Requests commits.
#It will show all the Commits date wise and sorted

anudan_app=/root/anudan-project/anudan-app/anudan-app-release-notes.txt
TAG_VER=/root/anudan-project/release/release.json
PROD_REL=`cat $TAG_VER | cut -d "," -f2 | cut -d ":" -f2 | sed 's/"//g'`
UAT_REL=`cat $TAG_VER | cut -d "," -f1 | cut -d ":" -f2 | sed 's/"//g'`
echo $PROD_REL
echo $UAT_REL

if [ -f "$anudan_app" ]
 then
   rm $anudan_app
fi

DATE=
git log $PROD_REL..$UAT_REL --pretty=format:"%ad || %h || %s || Author:%an || anudan-app-log " --date=short | sort -r | while read line
do
temp=`echo $line | egrep -v '(Automatic merge from|Merge pull request|Merge conflict from|Resolve Conflict From)'`
if [ "$temp" = "" ]
then
    continue
else
    NEWDATE=`echo $temp |  awk  '{print $1}'`
    if [ "$NEWDATE" = "$DATE" ]
    then
        echo $temp | awk '{$1="";$2="";print}' >> anudan-app-release-notes.txt
    else
        echo >> anudan-app-release-notes.txt
        DATE=$NEWDATE
        echo `date --date=$DATE +%d-%B-%Y` >> anudan-app-release-notes.txt
        echo $temp | awk '{$1="";$2="";print}' >> anudan-app-release-notes.txt
    fi
fi
done
echo "
" >> anudan-app-release-notes.txt
echo "--------------------------------------------------------------------------------------------" >> anudan-app-release-notes.txt
echo >> anudan-app-release-notes.txt
